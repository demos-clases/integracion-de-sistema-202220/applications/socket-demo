const express = require('express');
const app = express();
const http = require('http');

const { Server } = require("socket.io");
const server = http.createServer(app);
const path = require('path');

const io = new Server(server);


// app.use('/assets', express.static(__dirname + '../public'));
app.use('/assets', 
    express.static(
      path.resolve(__dirname, '../public'),
      { extensions: ['css', 'js', 'jpg'] }
    )
  );

// Se indica el directorio donde se almacenarán las plantillas 
app.set('views', './src/views');
// Se indica el motor del plantillas a utilizar
app.set('view engine', 'pug');

app.get('/', (req, res) => {
  const data = { name: 'Roberto'}
  res.render('index', data);
});


// socket zone
io.on("connection", (socket) => {
  console.log('navegador conectado',socket.id);
  socket.emit('message', 'Bienvenido al chat');
  socket.on('chat-general', (data) => {
    console.log('chat-general',data);
    io.emit('chat-general', data);
  });
});
io.on('disconnect', (socket) => {
  console.log('navegador desconectado',socket.id);
});

server.listen(3000, () => {
  console.log('Server started on port 3000');
});